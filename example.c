#include <stdio.h>
#include "yal_log.h"

int
main(int argc, char ** argv)
{
  init_logger();
  set_log_file("log");

  log(YAL_ERROR, "Error not showing up %s\n", "error 1");
  set_lvl_err();
  log(YAL_ERROR, "Error now showing up %s\n", "error 2");

  log(YAL_WARN, "Warning not showing up %s\n", "warning 1");
  set_lvl_warn();
  log(YAL_WARN, "Warning now showing up %s\n", "warning 2");


  log(YAL_DBG, "Debug info not showing up %s\n", "debug info 1");
  set_lvl_dbg();
  log(YAL_DBG, "Debug info now showing up %s\n", "debug info 2");

  
  log(YAL_INFO, "Info not showing up %s\n", "info 1");
  set_lvl_info();
  log(YAL_INFO, "Info now showing up %s\n", "info 2");
  close_log_file();
  return 0;
}
